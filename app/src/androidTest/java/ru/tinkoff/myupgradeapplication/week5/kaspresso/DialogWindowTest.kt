package ru.tinkoff.myupgradeapplication.week5.kaspresso

import androidx.test.ext.junit.rules.ActivityScenarioRule
import com.kaspersky.kaspresso.testcases.api.testcase.TestCase
import org.junit.Rule
import org.junit.Test
import ru.tinkoff.myupgradeapplication.MainActivity

class DialogWindowTest : TestCase() {
    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun checkOpenDialogWindow() {
        run {
            step("Open Dialog Window") {
                KaspressoFirstScreen {
                    dialogButton.click()
                    dialogWindowTitleViewMatchers.hasText("Важное сообщение")
                    dialogWindowMessageViewMatchers.hasText("Теперь ты автоматизатор")
                }
            }
        }
    }

    @Test
    fun checkCloseDialogWindow() {
        run {
            step("Open Dialog Window") {
                KaspressoFirstScreen {
                    dialogButton.click()
                    step("Close Dialog Window") {
                        pressBack()
                        dialogWindowTitleViewMatchers.doesNotExist()
                        dialogWindowTitleViewMatchers.doesNotExist()
                    }
                }
            }
        }
    }
}