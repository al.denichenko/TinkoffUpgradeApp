package ru.tinkoff.myupgradeapplication.week5.kaspresso

import android.view.View
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.DialogTitle
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.UiDevice
import com.google.android.material.appbar.MaterialToolbar
import com.google.android.material.snackbar.SnackbarContentLayout
import com.google.android.material.textview.MaterialTextView
import com.kaspersky.kaspresso.screens.KScreen
import io.github.kakaocup.kakao.text.KButton
import io.github.kakaocup.kakao.text.KTextView
import org.hamcrest.Matchers
import ru.tinkoff.myupgradeapplication.MainActivity
import ru.tinkoff.myupgradeapplication.R
import ru.tinkoff.myupgradeapplication.week5.kautomator.KautomatorLoginScreen

class KaspressoFirstScreen : BaseScreen() {
    val nextButton = KButton { withId(R.id.button_first) }
    val fab = KButton { withId(R.id.fab) }
    val changeButton = KButton { withId(R.id.change_button) }

    val textOnSnackBar = KTextView {
        isInstanceOf(MaterialTextView::class.java)
        withParent { isInstanceOf(SnackbarContentLayout::class.java) }
    }

    val screenTitle = KTextView {
        isInstanceOf(AppCompatTextView::class.java)
        withParent { isInstanceOf(MaterialToolbar::class.java) }
    }

    val dialogButton = KButton { withId(R.id.dialog_button) }
    val dialogWindowTitleViewMatchers =
        KTextView { isInstanceOf(DialogTitle::class.java) }
    val dialogWindowMessageViewMatchers =
        KTextView { isInstanceOf(MaterialTextView::class.java) }

    val textOnPage = KTextView { withId(R.id.textview_first) }

    companion object {
        inline operator fun invoke(crossinline block: KaspressoFirstScreen.() -> Unit) =
            KaspressoFirstScreen().block()
    }
}