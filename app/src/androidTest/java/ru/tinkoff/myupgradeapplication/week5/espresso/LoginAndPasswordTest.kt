package ru.tinkoff.myupgradeapplication.week5.espresso

import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import ru.tinkoff.myupgradeapplication.MainActivity
import ru.tinkoff.myupgradeapplication.week5.espresso.screens.EspressoFirstScreen
import ru.tinkoff.myupgradeapplication.week5.espresso.screens.EspressoLoginScreen

@RunWith(AndroidJUnit4::class)
class LoginAndPasswordTest {
    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun checkPasswordIsEmpty() {
        with(EspressoFirstScreen()) {
            clickFirstButton()
        }

        with(EspressoLoginScreen()) {
            typeTextToLoginFiled("qwe")
            clickSubmitButton()
            checkTextOnSnackBar("Password field must be filled!")
        }
    }

    @Test
    fun checkLoginIsEmpty() {
        with(EspressoFirstScreen()) {
            clickFirstButton()
        }

        with(EspressoLoginScreen()) {
            typeTextToPasswordFiled("123")
            clickSubmitButton()
            checkTextOnSnackBar("Login field must be filled!")
        }
    }

    @Test
    fun checkLoginAndPasswordAreEmpty() {
        with(EspressoFirstScreen()) {
            clickFirstButton()
        }
        with(EspressoLoginScreen()) {
            clickSubmitButton()
            checkTextOnSnackBar("Both of fields must be filled!")
        }
    }
}