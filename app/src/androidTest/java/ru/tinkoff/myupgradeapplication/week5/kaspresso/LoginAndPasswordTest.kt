package ru.tinkoff.myupgradeapplication.week5.kaspresso

import androidx.test.ext.junit.rules.ActivityScenarioRule
import com.kaspersky.kaspresso.testcases.api.testcase.TestCase
import org.junit.Rule
import org.junit.Test
import ru.tinkoff.myupgradeapplication.MainActivity

class LoginAndPasswordTest : TestCase() {
    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun checkPasswordIsEmpty() {
        run {
            val login = "qwe"
            step("Navigate to Login Screen") {
                KaspressoFirstScreen {
                    nextButton.click()
                }
            }
            step("Type text to Login field") {
                KaspressoLoginScreen {
                    editTextlogin.typeText(login)
                    step("Submit") {
                        buttonSubmit.click()
                        textOnSnackBar.hasText("Password field must be filled!")
                    }
                }
            }
        }
    }

    @Test
    fun checkLoginIsEmpty() {
        run {
            val password = "123"
            step("Navigate to Login Screen") {
                KaspressoFirstScreen {
                    nextButton.click()
                }
            }
            step("Type text to Password field") {
                KaspressoLoginScreen {
                    editTextpassword.typeText(password)
                    step("Submit") {
                        buttonSubmit.click()
                        textOnSnackBar.hasText("Login field must be filled!")
                    }
                }
            }
        }
    }

    @Test
    fun checkLoginAndPasswordAreEmpty() {
        run {
            step("Navigate to Login Screen") {
                KaspressoFirstScreen {
                    nextButton.click()
                }
            }
            step("Submit") {
                KaspressoLoginScreen {
                    buttonSubmit.click()
                    textOnSnackBar.hasText("Both of fields must be filled!")
                }
            }
        }
    }
}