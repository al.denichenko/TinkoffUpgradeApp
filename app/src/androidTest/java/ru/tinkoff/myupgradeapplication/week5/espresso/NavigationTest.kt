package ru.tinkoff.myupgradeapplication.week5.espresso

import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import ru.tinkoff.myupgradeapplication.MainActivity
import ru.tinkoff.myupgradeapplication.week5.espresso.screens.EspressoFirstScreen
import ru.tinkoff.myupgradeapplication.week5.espresso.screens.EspressoLoginScreen

@RunWith(AndroidJUnit4::class)
class NavigationTest {
    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun checkDefaultText() {
        with(EspressoFirstScreen()) {
            clickChangeButton()
            clickFirstButton()
        }

        with(EspressoLoginScreen()) {
            clickPreviousButton()
        }

        with(EspressoFirstScreen()) {
            checkTextOnFirstPage()
        }
    }

    @Test
    fun checkLoginAndPasswordAreEmpty() {
        val login = "qwe"
        val password = "123"

        with(EspressoFirstScreen()) {
            clickFirstButton()
        }

        with(EspressoLoginScreen()) {
            typeTextToLoginFiled(login)
            typeTextToPasswordFiled(password)
            clickPreviousButton()
        }

        with(EspressoFirstScreen()) {
            clickFirstButton()
        }

        with(EspressoLoginScreen()) {
            checkLoginAndPasswordAreEmpty("", "")
        }
    }
}