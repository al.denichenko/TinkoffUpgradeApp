package ru.tinkoff.myupgradeapplication.week5.espresso.screens

import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.DialogTitle
import androidx.core.content.res.TypedArrayUtils.getText
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.assertion.ViewAssertions.doesNotExist
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withParent
import androidx.test.espresso.matcher.ViewMatchers.withResourceName
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.UiDevice
import com.google.android.material.appbar.MaterialToolbar
import com.google.android.material.snackbar.SnackbarContentLayout
import com.google.android.material.textview.MaterialTextView
import com.kaspersky.components.kautomator.system.UiSystem.view
import io.github.kakaocup.kakao.common.utilities.getResourceColor
import org.hamcrest.Matchers
import org.hamcrest.Matchers.allOf
import org.hamcrest.Matchers.instanceOf
import ru.tinkoff.myupgradeapplication.FirstFragment
import ru.tinkoff.myupgradeapplication.R

class EspressoFirstScreen {
    private val device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())

    private val textfieldOnFirstPage = withId(R.id.textview_first)
    private val changeButtonMatcher = withId(R.id.change_button)
    private val dialogWindowTitleViewMatchers = Matchers.instanceOf<View>(DialogTitle::class.java)
    private val dialogWindowMessageViewMatchers = Matchers.instanceOf<View>(MaterialTextView::class.java)
    private val dialogButtonMatcher = withId(R.id.dialog_button)
    private val firstButtonMatcher = withId(R.id.button_first)
    private val snackBarTextViewMatcher = allOf(
        ViewMatchers.withParent(Matchers.instanceOf(SnackbarContentLayout::class.java)),
        Matchers.instanceOf(MaterialTextView::class.java)
    )
    private val fabMatcher = withId(R.id.fab)
    private val screenTitleMatcher = allOf(
        Matchers.instanceOf(AppCompatTextView::class.java),
        ViewMatchers.withParent(Matchers.instanceOf(MaterialToolbar::class.java))
    )

    fun clickFirstButton(){
        onView(firstButtonMatcher)
            .perform(click())
    }

    fun checkTextOnSnackBar(text: String) {
        onView(snackBarTextViewMatcher)
            .check(matches(withText(text)))
    }

    fun clickFab(){
        onView(fabMatcher)
            .perform(click())
    }

    fun checkScreenTitle(title : String) {
        onView(screenTitleMatcher)
            .check(matches(withText(title)))
    }

    fun pressBack() {
        device.pressBack()
    }

    fun checkDialogWindow() {
        onView(dialogWindowTitleViewMatchers)
            .check(doesNotExist())
    }

    fun checkTextOnFirstPage() {
        onView(textfieldOnFirstPage)
            .check(matches(withText("Exorcizo te, immundissime spiritus, omnis incursio adversarii, omne phantasma, omnis legio, in nomine Domini nostri Jesu Christi eradicare, et effugare ab hoc plasmate Dei. Ipse tibi imperat, qui te de supernis caelorum in inferiora terrae demergi praecepit. Ipse tibi imperat, qui mari, ventis, et tempestatibus impersvit. Audi ergo, et time, satana, inimice fidei, hostis generis humani, mortis adductor, vitae raptor, justitiae declinator, malorum radix, fomes vitiorum, seductor hominum, proditor gentium, incitator invidiae, origo avaritiae, causa discordiae, excitator dolorum: quid stas, et resistis, cum scias. Christum Dominum vias tuas perdere? Illum metue, qui in Isaac immolatus est, in joseph venumdatus, in sgno occisus, in homine cruci- fixus, deinde inferni triumphator fuit. Sequentes cruces fiant in fronte obsessi. Recede ergo in nomine Patris et Filii, et Spiritus Sancti: da locum Spiritui Sancto, per hoc signum sanctae Cruci Jesu Christi Domini nostri: Qui cum Patre et eodem Spiritu Sancto vivit et regnat Deus, Per omnia saecula saeculorum. Et cum spiritu tuo. Amen.")))
    }

    fun checkTextOnDialogWindow(title: String, message: String) {
        onView(dialogWindowTitleViewMatchers)
            .check(matches(withText(title)))
        onView(dialogWindowMessageViewMatchers)
            .check(matches(withText(message)))
    }

    fun clickChangeButton(){
        onView(changeButtonMatcher)
            .perform(click())
    }

    fun clickDialogButton(){
        onView(dialogButtonMatcher)
            .perform(click())
    }
}