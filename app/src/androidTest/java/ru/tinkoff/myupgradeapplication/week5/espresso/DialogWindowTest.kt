package ru.tinkoff.myupgradeapplication.week5.espresso

import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import ru.tinkoff.myupgradeapplication.MainActivity
import ru.tinkoff.myupgradeapplication.week5.espresso.screens.EspressoFirstScreen

@RunWith(AndroidJUnit4::class)
class DialogWindowTest {
    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun checkOpenDialogWindow() {
        with(EspressoFirstScreen()) {
            clickDialogButton()
            checkTextOnDialogWindow("Важное сообщение", "Теперь ты автоматизатор")
        }
    }

    @Test
    fun checkCloseDialogWindow() {
        with(EspressoFirstScreen()) {
            clickDialogButton()
            pressBack()
            checkDialogWindow()
        }
    }
}