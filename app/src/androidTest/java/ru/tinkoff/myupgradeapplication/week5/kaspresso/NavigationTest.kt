package ru.tinkoff.myupgradeapplication.week5.kaspresso

import androidx.test.ext.junit.rules.ActivityScenarioRule
import com.kaspersky.kaspresso.testcases.api.testcase.TestCase
import org.junit.Rule
import org.junit.Test
import ru.tinkoff.myupgradeapplication.MainActivity

class NavigationTest : TestCase() {
    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun checkDefaultText() {
        run {
            step("Change text on First Screen") {
                KaspressoFirstScreen {
                    changeButton.click()
                    step("Navigate to Login Screen") {
                        nextButton.click()
                    }
                }
            }
            step("Navigate to First Screen") {
                KaspressoLoginScreen {
                    buttonPrevious.click()
                }
            }
            step("Check text on First Screen") {
                KaspressoFirstScreen {
                    textOnPage.hasText(
                        "Exorcizo te, immundissime spiritus, omnis incursio adversarii, omne phantasma, omnis legio, in nomine Domini nostri Jesu Christi eradicare, et effugare ab hoc plasmate Dei. Ipse tibi imperat, qui te de supernis caelorum in inferiora terrae demergi praecepit. Ipse tibi imperat, qui mari, ventis, et tempestatibus impersvit. Audi ergo, et time, satana, inimice fidei, hostis generis humani, mortis adductor, vitae raptor, justitiae declinator, malorum radix, fomes vitiorum, seductor hominum, proditor gentium, incitator invidiae, origo avaritiae, causa discordiae, excitator dolorum: quid stas, et resistis, cum scias. Christum Dominum vias tuas perdere? Illum metue, qui in Isaac immolatus est, in joseph venumdatus, in sgno occisus, in homine cruci- fixus, deinde inferni triumphator fuit. Sequentes cruces fiant in fronte obsessi. Recede ergo in nomine Patris et Filii, et Spiritus Sancti: da locum Spiritui Sancto, per hoc signum sanctae Cruci Jesu Christi Domini nostri: Qui cum Patre et eodem Spiritu Sancto vivit et regnat Deus, Per omnia saecula saeculorum. Et cum spiritu tuo. Amen."
                    )
                }
            }
        }
    }

    @Test
    fun checkLoginAndPasswordAreEmpty() {
        run {
            val login = "qwe"
            val password = "123"
            step("Navigate to Login Screen") {
                KaspressoFirstScreen {
                    nextButton.click()
                }
            }
            step("Type Login and Password") {
                KaspressoLoginScreen {
                    editTextlogin.typeText(login)
                    editTextpassword.typeText(password)
                    step("Navigate to First Screen") {
                        buttonPrevious.click()
                    }
                }
            }
            step("Navigate to Login Screen") {
                KaspressoFirstScreen {
                    nextButton.click()
                }
            }
            step("Check Login and Password") {
                KaspressoLoginScreen {
                    editTextlogin.hasText("")
                    editTextpassword.hasText("")
                }
            }
        }
    }
}